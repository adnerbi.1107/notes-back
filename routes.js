const notes = require('./handlers/notes')

module.exports = (app) => {
    app.get('/notes', notes.findAll )
    app.post('/', notes.create )
}